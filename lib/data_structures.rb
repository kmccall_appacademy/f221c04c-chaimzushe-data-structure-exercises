# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  # your code goes here
  arr.max.to_i - arr.min.to_i
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  # your code goes here
  arr == arr.sort
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  # your code goes her
  str.downcase.scan(/a|e|i|o|u/).length
  #str.length - str.downcase.delete("aeiou").length
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  # your code goes here
  str.delete("aeiouAIOUE")
end


# HARD

# Write a method that returns the an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  # your code goes here
  int.to_s.split("").sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  # your code goes here
  str_array = str.downcase.split("")
  str_array.length != str_array.uniq.length
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  # your code goes here
  arr_str = arr.join("")
"(#{arr_str.slice(0,3)}) #{arr_str[3,3]}-#{arr_str[6..-1]}"
end
# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  # your code goes here
  str_array = str.split(",")
  range(str_array)
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1) #10
  offset = offset % arr.size
  reverse = (arr.size - offset)
  offset > 0 ? arr.drop(offset) + arr.take(offset) : arr.drop(reverse) + arr.take(reverse)
end
